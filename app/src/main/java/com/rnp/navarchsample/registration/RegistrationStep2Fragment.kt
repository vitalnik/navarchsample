package com.rnp.navarchsample.registration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.rnp.navarchsample.R
import kotlinx.android.synthetic.main.registration_step2_fragment.*

class RegistrationStep2Fragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.registration_step2_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        messageText.text = messageText.text.toString() + " " + (arguments?.getString("STEP") ?: "NO DATA")

        continueButton.setOnClickListener { v ->
            v.findNavController().navigate(R.id.action_registration_step2_fragment_to_mainActivity)
            requireActivity().finish()
        }

        backButton.setOnClickListener { v ->
            v.findNavController().popBackStack()
        }
    }

}
