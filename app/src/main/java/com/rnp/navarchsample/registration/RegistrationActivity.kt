package com.rnp.navarchsample.registration

import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.rnp.navarchsample.R

class RegistrationActivity : AppCompatActivity() {

    var navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.registration_activity)

        if (savedInstanceState == null) {
            // NavHostFragment is set up in XML
        }

        val navController = Navigation.findNavController(this, R.id.nav_host_fragment) // (nav_host_fragment as NavHostFragment).navController

        navController.addOnNavigatedListener { _, destination ->

            val dest: String = try {
                resources.getResourceName(destination.id)
            } catch (e: Resources.NotFoundException) {
                Integer.toString(destination.id)
            }

            Toast.makeText(this@RegistrationActivity, "Navigated to $dest",
                        Toast.LENGTH_SHORT).show()

            Log.d("NavigationActivity", "Navigated to $dest")
        }


    }

}
