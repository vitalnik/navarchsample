package com.rnp.navarchsample.registration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.rnp.navarchsample.R
import kotlinx.android.synthetic.main.registration_step1_fragment.*

class RegistrationStep1Fragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.registration_step1_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        continueButton.setOnClickListener { v ->

            val bundle = Bundle()
            bundle.putString("STEP", "STEP 2")

            v.findNavController().navigate(R.id.action_registration_step1_fragment_to_registration_step2_fragment, bundle)
        }

        backButton.setOnClickListener { _ ->
            requireActivity().onBackPressed()
        }

        buttonToMain.setOnClickListener {
            it.findNavController().navigate(R.id.action_registration_step1_fragment_to_mainActivity)
            requireActivity().finish()
        }

        // creating navigation click listener
//        buttonToMain.setOnClickListener(
//                Navigation.createNavigateOnClickListener(R.id.action_registration_step1_fragment_to_mainActivity)
//        )
    }

}


