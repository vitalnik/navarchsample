package com.rnp.navarchsample.main2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.rnp.navarchsample.R
import kotlinx.android.synthetic.main.nav_fragment_3.*


class NavFragment3 : Fragment() {

    private val title by lazy(LazyThreadSafetyMode.NONE) { arguments?.getString("title") ?: "" }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.nav_fragment_3, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        message.text = title

        openChildFragment.setOnClickListener {

            val navController = Navigation.findNavController(it)
            navController.navigate(R.id.action_navigation3_to_navigation3child)
        }

    }

}
