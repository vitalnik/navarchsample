package com.rnp.navarchsample.main2

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.rnp.navarchsample.R
import kotlinx.android.synthetic.main.main_activity2.*


class MainActivity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity2)

        val navController = findNavController(R.id.mainNavigationFragment)

        setupActionBarWithNavController(navController)

        bottomNavigation.setupWithNavController(navController)
    }

    override fun onSupportNavigateUp() =
            findNavController(R.id.mainNavigationFragment).navigateUp()


}
