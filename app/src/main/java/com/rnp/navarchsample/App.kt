package com.rnp.navarchsample

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import com.rnp.navarchsample.singleactivity.User

class App : Application() {

    lateinit var user: User

    var isDeeplink: Boolean = false

    lateinit var notificationManager: NotificationManager

    companion object {
        const val DEFAULT_CHANNEL_ID = "com.rnp.navarchsample.default_notification_channel"
    }

    override fun onCreate() {
        super.onCreate()

        user = User(applicationContext)

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        initNotificationChannels()
    }

    private fun initNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // default notification channel
            val channel2 = NotificationChannel(DEFAULT_CHANNEL_ID, "Default Notification channel", NotificationManager.IMPORTANCE_DEFAULT)

            channel2.description = "Description"

            notificationManager.createNotificationChannel(channel2)
        }
    }


}
