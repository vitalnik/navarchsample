package com.rnp.navarchsample.singleactivity.registration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.rnp.navarchsample.R
import com.rnp.navarchsample.singleactivity.BaseFragment
import kotlinx.android.synthetic.main.registration_step1_single_fragment.*

class RegistrationStep1SingleFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.registration_step1_single_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        continueButton.setOnClickListener { v ->

            val bundle = Bundle()
            bundle.putString("STEP", "STEP 2")

            v.findNavController().navigate(R.id.action_registrationStep1SingleFragment_to_registrationStep2SingleFragment, bundle)
        }

        // creating navigation click listener
//        buttonToMain.setOnClickListener(
//                Navigation.createNavigateOnClickListener(R.id.action_registration_step1_fragment_to_mainActivity)
//        )
    }

}


