package com.rnp.navarchsample.singleactivity.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.rnp.navarchsample.App
import com.rnp.navarchsample.R
import com.rnp.navarchsample.singleactivity.BaseFragment
import kotlinx.android.synthetic.main.main2_single_fragment.*

class Main2SingleFragment : BaseFragment() {

    lateinit var app: App

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        app = requireActivity().applicationContext as App

        return inflater.inflate(R.layout.main2_single_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        val intent = activity!!.intent
        if (intent.data != null) {
            if (intent.data.scheme == "https") {

                app.isDeeplink = true

//                var pathSegments = intent.data.pathSegments
//                if (pathSegments.size > 0) {
//                }
            }
            intent.data = null
        }

        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        logoutButton.setOnClickListener { v ->

            app.user.logout()


            if (app.isDeeplink) {

                val navBuilder = NavOptions.Builder()
                val navOptions = navBuilder
                        .setPopUpTo(R.id.startSingleFragment, true)
                        .build()
                Navigation.findNavController(v).navigate(R.id.action_main2SingleFragment_to_loginSingleFragment, null, navOptions)

            } else {
                Navigation.findNavController(v).navigate(R.id.action_main2SingleFragment_to_loginSingleFragment)
            }


        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {

                NavHostFragment.findNavController(this).popBackStack()

                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }


    override fun onDestroyView() {
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(false)

        super.onDestroyView()
    }

}
