package com.rnp.navarchsample.singleactivity.registration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.rnp.navarchsample.App
import com.rnp.navarchsample.R
import com.rnp.navarchsample.singleactivity.BaseFragment
import kotlinx.android.synthetic.main.registration_step3_single_fragment.*

class RegistrationStep3SingleFragment : BaseFragment() {

    lateinit var app: App

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        app = requireActivity().applicationContext as App

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.registration_step3_single_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // getting data passed in safe args
        val step = RegistrationStep3SingleFragmentArgs.fromBundle(arguments).step
        messageText.text = messageText.text.toString() + " " + step

        continueButton.setOnClickListener { v ->

            app.user.login("NewRobot")

            val bundle = Bundle()
            bundle.putString("username", app.user.userName)

//            val navBuilder = NavOptions.Builder()
//            val navOptions = navBuilder
//                    .setEnterAnim(R.anim.slide_in_from_right)
//                    .setExitAnim(R.anim.slide_out_to_left)
//                    .setPopExitAnim(R.anim.slide_out_to_left)
//                    .setPopUpTo(R.id.registrationStep1SingleFragment, true)
//                    .build()
//            Navigation.findNavController(v).navigate(R.id.action_registrationStep2SingleFragment_to_mainSingleFragment, null, navOptions)

            v.findNavController().navigate(R.id.action_registrationStep3SingleFragment_to_mainSingleFragment, bundle)
        }

        backButton.setOnClickListener { v ->
            v.findNavController().popBackStack()
        }

        backButton2.setOnClickListener { v ->
         //   v.findNavController().popBackStack()
         //   v.findNavController().popBackStack()

            v.findNavController().navigate(R.id.action_registrationStep3SingleFragment_to_registrationStep1SingleFragment)
        }


        // creating navigation click listener
//        buttonToMain.setOnClickListener(
//                Navigation.createNavigateOnClickListener(R.id.action_registration_step1_fragment_to_mainActivity)
//        )
    }

}


