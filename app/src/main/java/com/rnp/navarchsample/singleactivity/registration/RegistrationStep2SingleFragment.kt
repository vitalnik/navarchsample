package com.rnp.navarchsample.singleactivity.registration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.rnp.navarchsample.App
import com.rnp.navarchsample.R
import com.rnp.navarchsample.singleactivity.BaseFragment
import kotlinx.android.synthetic.main.registration_step2_single_fragment.*

class RegistrationStep2SingleFragment : BaseFragment() {

    lateinit var app: App

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        app = requireActivity().applicationContext as App

        return inflater.inflate(R.layout.registration_step2_single_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        messageText.text = messageText.text.toString() + " " + (arguments?.getString("STEP") ?: "NO DATA")

        continueButton.setOnClickListener { v ->

            val action = RegistrationStep2SingleFragmentDirections.actionRegistrationStep2SingleFragmentToRegistrationStep3SingleFragment()
            action.setStep("STEP 3")

            v.findNavController().navigate(action)
        }

        backButton.setOnClickListener { v ->
            v.findNavController().popBackStack()
        }
    }

}
