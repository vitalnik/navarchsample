package com.rnp.navarchsample.singleactivity

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (parentFragment != null && parentFragment?.childFragmentManager != null) {
            val fragments = parentFragment?.childFragmentManager?.fragments
//            for (fragment: Fragment in fragments!!) {
//            }
        }
    }
}
