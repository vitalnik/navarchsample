package com.rnp.navarchsample.singleactivity.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.rnp.navarchsample.App
import com.rnp.navarchsample.R
import com.rnp.navarchsample.singleactivity.BaseFragment
import kotlinx.android.synthetic.main.login_single_fragment.*

class LoginSingleFragment : BaseFragment() {

    lateinit var app: App

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        app = requireActivity().applicationContext as App

        return inflater.inflate(R.layout.login_single_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!app.user.userName.isEmpty()) {
            userNameText.setText(app.user.userName)
        }

        loginButton.setOnClickListener { view ->

            app.user.login(userNameText.text.toString())

            Navigation.findNavController(view).navigate(R.id.action_loginSingleFragment_to_mainSingleFragment)
        }

        registrationButton.setOnClickListener {

            Navigation.findNavController(view).navigate(R.id.action_loginSingleFragment_to_registrationStep1SingleFragment)

        }
    }

}
