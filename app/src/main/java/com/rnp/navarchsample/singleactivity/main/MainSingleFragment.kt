package com.rnp.navarchsample.singleactivity.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.rnp.navarchsample.App
import com.rnp.navarchsample.R
import com.rnp.navarchsample.singleactivity.BaseFragment
import kotlinx.android.synthetic.main.main_single_fragment.*


class MainSingleFragment : BaseFragment() {

    lateinit var app: App

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        app = requireActivity().applicationContext as App

        return inflater.inflate(R.layout.main_single_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button1.setOnClickListener { view ->
            Navigation.findNavController(view).navigate(R.id.action_mainSingleFragment_to_main1SingleFragment)
        }

        button2.setOnClickListener { view ->
            Navigation.findNavController(view).navigate(R.id.action_mainSingleFragment_to_main2SingleFragment)
        }

        logoutButton.setOnClickListener { view ->
            app.user.logout()

            Navigation.findNavController(view).navigate(R.id.action_mainSingleFragment_to_loginSingleFragment)

            // global actions
            //Navigation.findNavController(view).navigate(R.id.action_global_loginSingleFragment)
        }

        if (app.user.authenticated) {
            message.text = "Welcome, " + app.user.userName + "!"
        }
    }

}
