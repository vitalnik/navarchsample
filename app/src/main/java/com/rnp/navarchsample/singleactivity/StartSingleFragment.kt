package com.rnp.navarchsample.singleactivity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.rnp.navarchsample.App
import com.rnp.navarchsample.R
import kotlinx.android.synthetic.main.start_single_fragment.*

class StartSingleFragment : BaseFragment() {

    lateinit var app: App

    companion object {
        fun newInstance() = StartSingleFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        app = requireActivity().applicationContext as App

        return inflater.inflate(R.layout.start_single_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (app.user.authenticated && !app.isDeeplink) {

            val bundle = Bundle()
            bundle.putString("username", app.user.userName)

            NavHostFragment.findNavController(this).navigate(R.id.action_startSingleFragment_to_mainSingleFragment, bundle)
        } else {
            app.isDeeplink = false
        }

        loginButton.setOnClickListener { v ->
            Navigation.findNavController(v).navigate(R.id.action_startSingleFragment_to_loginSingleFragment)
            //Navigation.findNavController(v).navigate(R.id.action_global_loginSingleFragment)
        }

        registrationButton.setOnClickListener { v ->
            v.findNavController().navigate(R.id.action_startSingleFragment_to_registrationStep1SingleFragment)
        }

    }

}
