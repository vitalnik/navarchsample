package com.rnp.navarchsample.singleactivity

import android.content.Context
import android.content.Context.MODE_PRIVATE

class User(val context: Context) {

    var authenticated: Boolean = false
    var userName:String = ""

    init {
        authenticated = context.getSharedPreferences("navapp", MODE_PRIVATE)?.getBoolean("AUTHENTICATED", false) ?: false
        userName = context.getSharedPreferences("navapp", MODE_PRIVATE)?.getString("USERNAME", "") ?: ""
    }

    fun login(userName:String) {
        this.userName = userName
        authenticated = true

        persist()
    }

    fun logout() {
        userName = ""
        authenticated = false

        persist()
    }

    private fun persist() {
        context.getSharedPreferences("navapp", MODE_PRIVATE)?.edit()?.putBoolean("AUTHENTICATED", authenticated)?.apply()
        context.getSharedPreferences("navapp", MODE_PRIVATE)?.edit()?.putString("USERNAME", userName)?.apply()
    }

}
