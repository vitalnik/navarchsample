package com.rnp.navarchsample.login

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.rnp.navarchsample.R
import kotlinx.android.synthetic.main.login_fragment.*

class LoginFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.login_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loginButton.setOnClickListener { view ->

            activity?.getPreferences(Context.MODE_PRIVATE)?.edit()?.putBoolean("AUTHENTICATED", true)?.apply()

            val bundle = Bundle()
            bundle.putString("USER_NAME", userNameText.text.toString())

            // Navigation.findNavController(it).navigate(R.id.action_login_fragment_to_mainActivity)
            view.findNavController().navigate(R.id.action_login_fragment_to_mainActivity, bundle)

            requireActivity().finish()
        }
    }

}
