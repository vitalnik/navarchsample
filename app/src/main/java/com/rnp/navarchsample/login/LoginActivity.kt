package com.rnp.navarchsample.login

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import com.rnp.navarchsample.R

class LoginActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)

        if (savedInstanceState == null) {
            val navHost = NavHostFragment.create(R.navigation.nav_graph_login)
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, navHost)
                    .setPrimaryNavigationFragment(navHost) // this is the equivalent to app:defaultNavHost="true"
                    .commit()
        }
    }

}
