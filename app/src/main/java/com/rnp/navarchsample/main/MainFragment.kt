package com.rnp.navarchsample.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.rnp.navarchsample.R
import com.rnp.navarchsample.login.LoginActivity
import kotlinx.android.synthetic.main.main_fragment.*



class MainFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        button1.setOnClickListener { view ->
            Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_blankFragment1)
        }

        button2.setOnClickListener { view ->
            Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_blankFragment2)
        }

        logoutButton.setOnClickListener { view ->

            activity?.getPreferences(Context.MODE_PRIVATE)?.edit()?.putBoolean("AUTHENTICATED", false)?.apply()

//            val navBuilder = NavOptions.Builder()
//            val navOptions = navBuilder.setClearTask(true).build()
//            Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_loginActivity, null, navOptions)

            Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_loginActivity)

            requireActivity().finish()
        }

        logoutButton1.setOnClickListener { view ->

            activity?.getPreferences(Context.MODE_PRIVATE)?.edit()?.putBoolean("AUTHENTICATED", false)?.apply()

            val intent = Intent(activity, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            activity?.startActivity(intent)

            //requireActivity().finish()
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        message.text = "Welcome, " + (arguments?.getString("USER_NAME") ?: "NONAME") + "!"

    }

}
