package com.rnp.navarchsample.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import com.rnp.navarchsample.R
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : AppCompatActivity() {

    private lateinit var navHost: NavHostFragment

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        // https://stackoverflow.com/questions/50334550/navigation-architecture-component-passing-argument-data-to-the-startdestination

        navHost = mainNavigationFragment as NavHostFragment

        val inflater = navHost.navController.navInflater
        val graph = inflater.inflate(R.navigation.nav_graph_main)

        val bundle = Bundle()

        bundle.putString("USER_NAME", intent?.extras?.getString("USER_NAME"))

        graph.addDefaultArguments(bundle)

        navHost.navController.graph = graph

//        if (savedInstanceState == null) {
//            supportFragmentManager.beginTransaction()
//                    .replace(R.id.container, navHost)
//                    .setPrimaryNavigationFragment(navHost) // this is the equivalent to app:defaultNavHost="true"
//                    .commit()
//        }
    }

}
