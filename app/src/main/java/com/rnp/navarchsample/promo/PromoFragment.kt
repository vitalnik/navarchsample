package com.rnp.navarchsample.promo

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.Fragment
import androidx.navigation.NavDeepLinkBuilder
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.rnp.navarchsample.App
import com.rnp.navarchsample.R
import com.rnp.navarchsample.main2.MainActivity2
import kotlinx.android.synthetic.main.promo_fragment.*

class PromoFragment : Fragment() {

    companion object {
        fun newInstance() = PromoFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.promo_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        loginButton.setOnClickListener { view ->

            //NavHostFragment.findNavController(this@PromoFragment).navigate(R.id.action_promo_fragment_to_loginActivity)

            // using Kotlin extension function "findNavController"
            // same as calling:
            Navigation.findNavController(view).navigate(R.id.action_promo_fragment_to_loginActivity)
            //view.findNavController().navigate(R.id.action_promo_fragment_to_loginActivity)

            requireActivity().finish()
        }

        buttonBottomNav.setOnClickListener { view ->

            val intent = Intent(requireActivity(), MainActivity2::class.java)

            //intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            //intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

            requireActivity().startActivity(intent)
            requireActivity().finish()
        }

        registrationButton.setOnClickListener { view ->
            view.findNavController().navigate(R.id.action_promo_fragment_to_registrationActivity)

            requireActivity().finish()
        }

        singleActivityButton.setOnClickListener { view ->
            Navigation.findNavController(view).navigate(R.id.action_promo_fragment_to_singleActivity)
            requireActivity().finish()
        }

        deepLinkButton.setOnClickListener {

            // Prepare the arguments to pass in the notification
            val arguments = Bundle().apply {
                putString("stringKey", "a string value")
                putInt("intKey", 999)
            }

            val pendingIndent = NavDeepLinkBuilder(requireActivity())
                    .setGraph(R.navigation.nav_graph_main)
                    .setDestination(R.id.blankFragment1)
                    .setArguments(arguments)
                    .createPendingIntent()

            val notification = NotificationCompat.Builder(requireActivity(), App.DEFAULT_CHANNEL_ID)
                    .setSmallIcon(R.drawable.notification)
                    .setContentTitle("Explicit deep link")
                    .setContentText("Clicking on this notification will take you to the destination fragment")
                    .setContentIntent(pendingIndent)
                    .setAutoCancel(true)
                    .build()

            NotificationManagerCompat.from(requireActivity()).notify(10, notification)
        }

    }

}
