package com.rnp.navarchsample.promo

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import com.rnp.navarchsample.R

class PromoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.promo_activity)

        if (savedInstanceState == null) {

            val navHost = NavHostFragment.create(R.navigation.nav_graph_promo)

            // creating NavHostFragment programmatically

            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, navHost)
                    .setPrimaryNavigationFragment(navHost) // this is the equivalent to app:defaultNavHost="true"
                    .commit()
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        Toast.makeText(this, "onNewIntent delivered", Toast.LENGTH_SHORT).show()
    }
}
